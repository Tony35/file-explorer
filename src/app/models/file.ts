export class File {
    id: string;
    name: string;
    creation: Date;
    modification: Date;
    text: string;
  }
