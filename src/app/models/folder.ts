export class Folder {
    id: string;
    name: string;
    creation: Date;
    modification: Date;
  }
