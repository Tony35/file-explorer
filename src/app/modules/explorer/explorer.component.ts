import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { File } from '../../models/file';
import { Folder } from '../../models/folder';

import { CreateDialogComponent } from '../create-dialog/create-dialog.component';

@Component({
  selector: 'app-explorer',
  templateUrl: './explorer.component.html',
  styleUrls: ['./explorer.component.scss']
})
export class ExplorerComponent implements OnInit {
  elements = {};
  contains = {};
  belongs = {};
  currentRoute = 'root';
  idFolder = 1;
  idFile = 1;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.init();
  }

  pathname() {
    let elem = this.currentRoute;
    let path = this.elements[elem] ? this.elements[elem].name : '/';
    while (elem !== 'root') {
      elem = this.belongs[elem];
      path = (this.elements[elem] ? this.elements[elem].name : '') + '/' + path;
    }
    return path;
  }

  open(element) {
    if (element.substr(0, 1) === 'd') {
      this.currentRoute = element;
    } else {
      console.log(this.elements[element].text);
    }
  }

  goBack() {
    this .currentRoute = this.belongs[this.currentRoute] ? this.belongs[this.currentRoute] : 'root';
  }

  init() {
    // tslint:disable: max-line-length
    // tslint:disable: no-string-literal
    this.elements['d1'] = {id: 'd1', name: 'Documents', creation: new Date('December 17, 2018 13:24:00'), modification: new Date('September 12, 2019 12:45:10')};
    this.sortElement(this.elements['d1']);
    this.elements['d2'] = {id: 'd2', name: 'Photos', creation: new Date('December 5, 2019 03:24:00'), modification: new Date('December 5, 2019 03:24:00')};
    this.sortElement(this.elements['d2'], this.elements['d1']);
    this.elements['d3'] = {id: 'd3', name: 'Musiques', creation: new Date('December 5, 2019 03:24:00'), modification: new Date('December 5, 2019 03:24:00')};
    this.sortElement(this.elements['d3'], this.elements['d1']);
    this.elements['d4'] = {id: 'd4', name: 'Vidéos', creation: new Date('January 23, 2019 08:00:00'), modification: new Date('February 2, 2019 12:35:02')};
    this.sortElement(this.elements['d4']);
    this.idFolder = 4;

    this.elements['f1'] = {id: 'f1', name: 'test.txt', creation: new Date('November 17, 2019 17:54:00'), modification: new Date('November 17, 2019 17:55:38'), text: 'Mon 1er fichier'};
    this.sortElement(this.elements['f1'], this.elements['d2']);
    this.elements['f2'] = {id: 'f2', name: 'documents.txt', creation: new Date('April 20, 2018 07:22:10'), modification: new Date('April 20, 2018 23:49:12'), text: 'Ce fichier se trouve à la racine de mon serveur'};
    this.sortElement(this.elements['f2']);
    this.idFile = 3;
  }

  /**
   * Folder Constructor
   *
   * @param folderName folderName : the name of the new directory
   * @param destination destination for store the new folder
   */
  createFolder(folderName: string, destination?: Folder) {
    const ident = 'd' + this.idFolder;
    const date = new Date(Date.now());
    this.elements[ident] = {id: ident, name: folderName, creation: date, modification: date};
    this.sortElement(this.elements[ident], (destination ? destination : null));
    this.idFolder++;
  }

  /**
   * File constructor
   *
   * @param fileName fileName : the name of the new file
   * @param destination destination for store the new file
   */
  createFile(fileName: string, txt: string, destination?: Folder) {
    const ident = 'f' + this.idFolder;
    const date = new Date(Date.now());
    this.elements[ident] = {id: ident, name: fileName + '.txt', creation: date, modification: date, text: txt};
    this.sortElement(this.elements[ident], (destination ? destination : null));
    this.idFile++;
  }

  /**
   * Sort element in the tree
   *
   * @param element element to tidy up
   * @param destination destination folder, if empty destination is 'root'
   */
  sortElement(element: Folder | File, destination?: Folder) {
    const dest = destination ? destination.id : 'root';
    const tmp = this.contains[dest] ? this.contains[dest] : [];
    tmp.push(element.id);
    this.contains[dest] = tmp;
    this.belongs[element.id] = dest;
  }


  openCreateDialog(element: string) {
    const dialogRef = this.dialog.open(CreateDialogComponent,
    {
      data: {
        title: (element === 'file' ? 'fichier' : 'dossier'),
        name: '',
        text: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        const destination = (this.currentRoute !== 'root' ? this.elements[this.currentRoute] : null);
        if (element === 'folder') {
          this.createFolder(result.name, destination);
        } else {
          this.createFile(result.name, result.text, destination);
        }
      }
    });
  }

}
